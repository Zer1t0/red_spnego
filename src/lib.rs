//! A crate to play with SPNEGO
//!


mod oid;
pub use oid::{
    krb5_ms_oid, krb5_oid, krb5_user_oid, negoex_oid, ntlmssp_oid, spnego_oid,
};

mod state;
pub use state::{ACCEPT_COMPLETED, ACCEPT_INCOMPLETE, REJECT, REQUEST_MIC};

mod init2;
pub use init2::{NegHints, NegTokenInit2};

mod negotiation_token;
pub use negotiation_token::NegToken;

mod resp;
pub use resp::NegTokenResp;
