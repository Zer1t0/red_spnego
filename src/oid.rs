use red_asn1::Oid;

pub fn spnego_oid() -> Oid {
    Oid::new(1, 3, vec![6, 1, 5, 5, 2])
}

pub fn ntlmssp_oid() -> Oid {
    Oid::new(1, 3, vec![6, 1, 4, 1, 311, 2, 2, 10])
}

pub fn krb5_user_oid() -> Oid {
    Oid::new(1, 2, vec![840, 113554, 1, 2, 2, 3])
}

pub fn krb5_oid() -> Oid {
    Oid::new(1, 2, vec![840, 113554, 1, 2, 2])
}

pub fn krb5_ms_oid() -> Oid {
    Oid::new(1, 2, vec![840, 48018, 1, 2, 2])
}

pub fn negoex_oid() -> Oid {
    Oid::new(1, 3, vec![6, 1, 4, 1, 311, 2, 2, 30])
}
