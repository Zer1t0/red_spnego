pub const ACCEPT_COMPLETED: u32 = 0x0;
pub const ACCEPT_INCOMPLETE: u32 = 0x1;
pub const REJECT: u32 = 0x2;
pub const REQUEST_MIC: u32 = 0x3;
